/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "dma.h"
#include "i2c.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "oled.h"
#include "bmp.h"
#include "lkdGui.h"
extern  lkdFont defaultFont;
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
void test1WinInit(void);

static lkdWin test1Win,MainWin;
void MainWinInit(void);

void test1Winfun(void)
{
	/* 窗口内容具体代码实现 */
	static uint8_t flag;
	static uint32_t testTick;
	static lkdProgress progress1;
	if(flag == 0){
		progress1.x = 5;
		progress1.y = 0;
		progress1.ratio = 0;
		progress1.wide = 60;
		progress1.high = 40;
		flag = 1;
	}
	if(flag == 1){
		GuiProGress(&progress1);
		GuiUpdateDisplayAll();
		testTick = HAL_GetTick();
		flag = 2;
	}
	if(flag == 2){
		if(HAL_GetTick()-testTick > 1000){//两秒刷新一次
			progress1.ratio += 5;
			flag = 1;
			if(progress1.ratio >= 100){
				flag = 3;
			}
		}
	}
	if(flag == 3){
		GuiWinDeleteTop();
		GuiUpdateDisplayAll();
		flag = 0;
	}
}

void test1WinInit(void)
{
	test1Win.x = 0;
	test1Win.y = 0;
	test1Win.hight = 64;
	test1Win.wide = 28;
	test1Win.title = "test1";
	test1Win.WindowFunction = test1Winfun;
}

void MainWinfun(void)
{
	/* 窗口内容具体代码实现 */
	static uint8_t flag;
	static uint32_t testTick;
	if(flag == 0){
		flag = 1;
	}
	//if(flag == 1){
		GuiRowText(0, 10,128, FONT_MID, "main");
		GuiUpdateDisplayAll();
		testTick = HAL_GetTick();
		flag = 2;
	//}
//	if(flag == 2){
//		if(HAL_GetTick()-testTick > 5000){//两秒刷新一次
//			test1WinInit();
//			GuiWinAdd(&test1Win);
//			flag = 0;
//		}
//	}
}


/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_I2C1_Init();
  /* USER CODE BEGIN 2 */
	
	uint8_t t=0;											   
OLED_Init();				//初始化OLED 

void defaultFontInit(void);
	defaultFontInit();/* 字库初始化 */


	
	/* -----------1--------------- */
	fontTextInfo textInfo;
	textInfo.x = 0;/* 文本起始坐标 */
	textInfo.y = 0;
	textInfo.wide = 64;/* 文本范围大小 */
	textInfo.high = 128;
	textInfo.wInterval = 0;/* 字符间距 */
	textInfo.hInterval = 2;/* 行间距 */
	textInfo.flag = 0;/* 不反显 */
	textInfo.beginOffset = textInfo.wide * 2;/* 开始偏移,首行缩进 */
	GuiText(&textInfo, "abc123");
	GuiUpdateDisplayAll();
	HAL_Delay(2000);
	GuiClearScreen(0);
	
//	lkdButton tButton;
//	tButton.x = 10;
//	tButton.y = 10;
//	tButton.wide = 40;
//	tButton.high = 30;
//	tButton.name = "ok";
//	tButton.flag = BUTTON_DOWN;/* 抬起状态 */
//	GuiButton(&tButton);
//	GuiUpdateDisplayAll();
//	HAL_Delay(2000);
//	GuiClearScreen(0);
//	
//	lkdScroll thScroll,tvScroll;
//	thScroll.x = 10;
//	thScroll.y = 10;
//	thScroll.hight = 10;
//	thScroll.max=100;
//	thScroll.lump=10;
//	
//	tvScroll.x=0;
//	tvScroll.y=0;
//	tvScroll.max= 64;
//	tvScroll.lump = 5;
//	HAL_Delay(2000);
//	

//	
//	lkdProgress process;
//	process.x=0;
//	process.y=0;
//	process.high=30;
//	process.wide=60;
//	//while(1)
//	{

//			for(uint8_t i = 0; i < thScroll.max; i ++){
//				thScroll.hight = i;/* 进度快控制 */
//	
//				GuiHScroll(&thScroll);/* 垂直进度条 */
//				GuiUpdateDisplayAll();/* 更新 */
//				HAL_Delay(20);
//			}
//			GuiClearScreen(0);
//			
//			
//			for(uint8_t i = 0; i < tvScroll.max; i ++){
//				tvScroll.hight = i;/* 进度快控制 */
//				GuiVScroll(&tvScroll);/* 水平进度条 */
//				GuiUpdateDisplayAll();/* 更新 */
//				HAL_Delay(20);
//			}
//			GuiClearScreen(0);
//			
//			for(uint8_t i = 0; i < process.wide; i ++){
//				process.ratio = i;/* 进度快控制 */
//				GuiProGress(&process);/* 水平进度条 */
//				GuiUpdateDisplayAll();/* 更新 */
//				HAL_Delay(20);
//			}
//			HAL_Delay(2000);
//			GuiClearScreen(0);
//	}
	
//	lkdMenuNode node={
//		.nodeId=0,
//		.name="root"
//	};
//	lkdMenuNode node_c1={
//		.nodeId=10,
//		.name="c1"
//	};
	//1. 定义一个菜单项
	#define MENUSTACK_NUM 8
	MenuStack userMenuStack[MENUSTACK_NUM];
	
	//2. 定义二级菜单
	lkdMenuNode Node3_3 = {6, "no1-3", NULL, NULL,NULL };     // 信息
	lkdMenuNode Node2_2 = {5, "no1-2", &Node3_3, NULL,NULL };     // 信息
	lkdMenuNode Node1_1 = {4, "no1-1", &Node2_2, NULL,NULL };  // 参数设置
	
	//3. 定义一级菜单
	lkdMenuNode Node3 = {3, "no2", NULL, NULL,NULL };     // 信息
	lkdMenuNode Node2 = {2, "no2", &Node3, NULL,NULL };     // 信息
	lkdMenuNode Node1 = {1, "no1", &Node2, &Node1_1,NULL };  // 参数设置
	
	//3. 定义根菜单
	lkdMenuNode NodeRoot0 = {0, "root", NULL, &Node1, NULL};

	//初始化根节点
	lkdMenu root={
		.x=0,
		.y=0,
		.wide=128,
		.hight=64,
		.ItemsWide=48,
		.Itemshigh=15,
		.index=1,//默认选中节点
		.stackNum=MENUSTACK_NUM,
		.stack = userMenuStack,
		.Root = &NodeRoot0
	};
	
	
	
	//初始化菜单
	GuiMenuInit(&root);
	//展开当前选中节点
	GuiMenuCurrentNodeSonUnfold(&root);
	
	
	GuiUpdateDisplayAll();/* 更新 */
	HAL_Delay(2000);
	
	GuiMenuCurrentNodeSonUnfold(&root);
	GuiUpdateDisplayAll();/* 更新 */
	HAL_Delay(2000);
	
	
	GuiMenuItemDownMove(&root);//选中下移
	GuiUpdateDisplayAll();/* 更新 */
	HAL_Delay(2000);
	GuiClearScreen(0);
	
	
	
	
	
	//构造一个窗口
	MainWin.x = 0;
	MainWin.y = 0;
	MainWin.hight = 64;
	MainWin.wide = 128;
	MainWin.title = NULL;
	MainWin.WindowFunction = MainWinfun;//窗口处理函数
	//添加一个窗口
	GuiWinAdd(&MainWin);
	while(1)
	{
		//循环调用窗口处理
		GuiWinDisplay();
	}
while(1) 
	{
//		OLED_ShowPicture(0,0,128,8,BMP1);
//		HAL_Delay(500);
//		OLED_Clear();
//		OLED_ShowChinese(0,0,0,16);//中
//		OLED_ShowChinese(18,0,1,16);//景
//		OLED_ShowChinese(36,0,2,16);//园
//		OLED_ShowChinese(54,0,3,16);//电
//		OLED_ShowChinese(72,0,4,16);//子
//		OLED_ShowChinese(90,0,5,16);//科
//		OLED_ShowChinese(108,0,6,16);//技
//		OLED_ShowString(8,16,"ZHONGJINGYUAN",16);
//		OLED_ShowString(20,32,"2014/05/01",16);
//		OLED_ShowString(0,48,"ASCII:",16);  
//		OLED_ShowString(63,48,"CODE:",16);
//		OLED_ShowChar(48,48,t,16);//显示ASCII字符	   
//		t++;
//		if(t>'~')t=' ';
//		OLED_ShowNum(103,48,t,3,16);
//		
//		OLED_Refresh();
//		//while(1);
//		HAL_Delay(500);
//		OLED_Clear();
//		OLED_ShowChinese(0,0,0,16);  //16*16 中
//	  OLED_ShowChinese(16,0,0,24); //24*24 中
//		OLED_ShowChinese(24,20,0,32);//32*32 中
//	  OLED_ShowChinese(64,0,0,64); //64*64 中
//		OLED_Refresh();
//	  HAL_Delay(500);
//  	OLED_Clear();
//		OLED_ShowString(0,0,"ABC",12);//6*12 “ABC”
//	  OLED_ShowString(0,12,"ABC",16);//8*16 “ABC”
//		OLED_ShowString(0,28,"ABC",24);//12*24 “ABC”
//	  OLED_Refresh();
//		HAL_Delay(500);
//		OLED_ScrollDisplay(11,4);
	}
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
